<?php

$lang['flexshare_accessibility_invalid'] = 'Доступність недійсна.';
$lang['flexshare_access_lan'] = 'Лише LAN';
$lang['flexshare_allow_unencrypted_ftp'] = 'Дозволити незашифрований FTP';
$lang['flexshare_app_description'] = 'Flexshares — це гнучкі ресурси спільного доступу, які дозволяють адміністратору швидко й легко визначати зони спільного використання даних, для співпраці та доступу до файлов через web та FTP.';
$lang['flexshare_app_name'] = 'Flexshare';
$lang['flexshare_config_validation_failed'] = 'Помилка оновлення – неможливо перевірити нову конфігурацію – перевірте журнали для отримання додаткової інформації.';
$lang['flexshare_description_invalid'] = 'Опис недійсний.';
$lang['flexshare_device_busy'] = 'A user or service is accessing the current share.';
$lang['flexshare_directory'] = 'Directory';
$lang['flexshare_directory_invalid'] = 'Directory is invalid.';
$lang['flexshare_file_audit_log'] = 'Журнал аудиту';
$lang['flexshare_file_comment'] = 'Коментар';
$lang['flexshare_file_comment_invalid'] = 'File comment is invalid.';
$lang['flexshare_file_recyle_bin'] = 'Сміттєва корзина';
$lang['flexshare_file_server_url'] = 'URL-адреса сервера';
$lang['flexshare_flexshare'] = 'Flexshare';
$lang['flexshare_flexshares'] = 'Flexshares';
$lang['flexshare_flexshare_storage'] = 'Flexshare сховище';
$lang['flexshare_folder_layout_invalid'] = 'Folder layout is invalid.';
$lang['flexshare_ftp_and_ftpes_port'] = 'FTP і FTPES порт';
$lang['flexshare_ftp_cannot_use_default_ports'] = 'Стандартні порти FTP/FTPS (21, 990) зарезервовані і не можуть використовуватися Flexshares.';
$lang['flexshare_ftp'] = 'FTP';
$lang['flexshare_ftp_port_conflict'] = 'The custom FTP port conflicts with another defined flexshare - you must have allow for both the control port/channel (N) and data port/channel (N-1).';
$lang['flexshare_ftps_port'] = 'FTPS порт';
$lang['flexshare_greeting'] = 'Привітання';
$lang['flexshare_greeting_invalid'] = 'Привітання недійсне.';
$lang['flexshare_group'] = 'Група';
$lang['flexshare_group_invalid'] = 'Група недійсна.';
$lang['flexshare_name_invalid'] = 'Ім’я недійсне.';
$lang['flexshare_name'] = 'Імʼя';
$lang['flexshare_name_overlaps_with_group'] = 'Flexshare ім’я збігається з назвою групи.';
$lang['flexshare_name_overlaps_with_username'] = 'Flexshare ім’я збігається з іменем користувача.';
$lang['flexshare_no_access'] = 'Перш ніж увімкнути Flexshare, необхідно ввімкнути один або кілька параметрів доступу.';
$lang['flexshare_non_custom_port_warning'] = 'Default port is not valid when specifying a custom port.';
$lang['flexshare_options'] = 'Параметри';
$lang['flexshare_override_port'] = 'Перевизначити порт';
$lang['flexshare_owner_invalid'] = 'Власник недійсний.';
$lang['flexshare_passive_mode_from_port'] = 'Пасивний режим, порт з';
$lang['flexshare_passive_mode'] = 'Пасивний режим';
$lang['flexshare_passive_mode_to_port'] = 'Пасивний режим, порт до';
$lang['flexshare_passive_port_below_min'] = 'Діапазон портів нижче мінімуму 1024.';
$lang['flexshare_permissions_invalid'] = 'Дозволи недійсні.';
$lang['flexshare_permissions'] = 'Дозволи';
$lang['flexshare_port_already_in_use'] = 'Порт уже використовується.';
$lang['flexshare_port_invalid'] = 'Недійсний порт.';
$lang['flexshare_port'] = 'Номер порту';
$lang['flexshare_port_range_invalid'] = 'Діапазон портів недійсний.';
$lang['flexshare_ports'] = 'Порти';
$lang['flexshare_read_only'] = 'Лише читати';
$lang['flexshare_read_write'] = 'Читати/Писати';
$lang['flexshare_sandbox'] = 'Sandbox';
$lang['flexshare_server_url_alt'] = 'URL альтернативного сервера';
$lang['flexshare_server_url'] = 'URL сервера';
$lang['flexshare_share_already_exists'] = 'Flexshare вже існує.';
$lang['flexshare_share_name'] = 'Імʼя шари';
$lang['flexshare_share_not_found'] = 'Шара не знайдена.';
$lang['flexshare_standard'] = 'Стандарт';
$lang['flexshare_summary'] = 'Summary';
$lang['flexshare_third_party_app_access'] = 'Доступ сторонніх програм';
$lang['flexshare_web_accessibility'] = 'Доступність';
$lang['flexshare_web_allow_htaccess'] = 'Використовувати [.htaccess]';
$lang['flexshare_web_allow_ssi'] = 'Дозволити Server Side Includes';
$lang['flexshare_web_enable_cgi'] = 'Увімкнути CGI';
$lang['flexshare_web_enable_php'] = 'Увімкніть PHP';
$lang['flexshare_web_folder_layout'] = 'Макет папки';
$lang['flexshare_web_follow_symlinks'] = 'Follow Symlinks / Allow Rewrite';
$lang['flexshare_web_override_default_port'] = 'Замінити порт за замовчуванням';
$lang['flexshare_web_realm_invalid'] = 'Realm is invalid.';
$lang['flexshare_web_realm'] = 'Realm';
$lang['flexshare_web_require_authentication'] = 'Вимагати аутентифікацію';
$lang['flexshare_web_require_ssl'] = 'Потрібен SSL / HTTPS';
$lang['flexshare_web_server_name'] = 'Імʼя сервера';
$lang['flexshare_web_show_index'] = 'Показувати індекс';
$lang['flexshare_web'] = 'Web';
$lang['flexshare_windows_file_share'] = 'Спільний доступ до файлів Windows';
$lang['flexshare_php_engine'] = 'PHP Engine';
